package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfig_String(t *testing.T) {
	c := &Config{}
	assert.Equal(t, `{"GitlabEndpoint":"","GitlabToken":"","GitlabUsername":"","GitlabPassword":""}`, c.String())
}

func TestLoadConfigFromEnv(t *testing.T) {
	c, err := LoadConfigFromEnv()
	assert.NoError(t, err)
	assert.Equal(t, "https://gitlab.com/api/v4", c.GitlabEndpoint)
}
