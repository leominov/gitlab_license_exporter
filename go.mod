module github.com/leominov/gitlab_license_exporter

go 1.14

require (
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/prometheus/client_golang v1.7.0
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.4.0
	github.com/xanzy/go-gitlab v0.32.2-0.20200619221245-41995da14d77
)
