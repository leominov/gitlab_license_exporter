package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"os/exec"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func fetchMetricsPage() (string, error) {
	resp, err := http.Get("http://127.0.0.1:9201/metrics")
	if err != nil {
		return "", err
	}
	reader := bufio.NewReader(resp.Body)
	var lines []string
	for {
		line, _, err := reader.ReadLine()
		if err != nil {
			break
		}
		if !bytes.Contains(line, []byte("gitlab_")) {
			continue
		}
		lines = append(lines, string(line))
	}
	return strings.Join(lines, "\n"), err
}

func TestFixtures(t *testing.T) {
	tests := []struct {
		cas        string
		incVersion bool
	}{
		{
			cas:        "case1",
			incVersion: false,
		},
		{
			cas:        "case2",
			incVersion: true,
		},
	}
	for _, test := range tests {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			switch r.URL.Path {
			case "/api/v4/license":
				bytes, err := ioutil.ReadFile(fmt.Sprintf("test_data/%s/in.json", test.cas))
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				w.Write(bytes)
			case "/api/v4/version":
				w.Write([]byte(`{"version": "ABC", "revision": "DEF"}`))
			}
		}))

		_, err := exec.Command("go", "build", "./").CombinedOutput()
		if !assert.NoError(t, err) {
			return
		}

		cmd := exec.Command("./gitlab_license_exporter")
		if test.incVersion {
			cmd = exec.Command("./gitlab_license_exporter", "--collect-version")
		}
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		cmd.Env = []string{
			fmt.Sprintf("GITLAB_TOKEN=ABCD"),
			fmt.Sprintf("GITLAB_ENDPOINT=%s", server.URL),
		}
		err = cmd.Start()
		if !assert.NoError(t, err) {
			return
		}

		var page string
		for i := 0; i < 5; i++ {
			page, err = fetchMetricsPage()
			if err == nil {
				break
			}
			time.Sleep(100 * time.Millisecond)
		}

		server.Close()
		cmd.Process.Kill()

		out, err := ioutil.ReadFile(fmt.Sprintf("test_data/%s/out.txt", test.cas))
		if !assert.NoError(t, err) {
			return
		}
		result := string(out)
		result = strings.TrimSpace(result)

		assert.Equal(t, result, page)
	}
}
