# GitLab License Exporter

[![Build Status](https://travis-ci.com/leominov/gitlab_license_exporter.svg?branch=master)](https://travis-ci.com/leominov/gitlab_license_exporter)

## Environment variables

* `GITLAB_ENDPOINT` (ex.: `https://gitlab.local/api/v4`)
* `GITLAB_TOKEN`
* `GITLAB_USERNAME`
* `GITLAB_PASSWORD`

## Usage

```shell
Usage of ./gitlab_license_exporter:
  -collect-version
    	Collect GitLab version information.
  -log-level string
    	Level of logging. (default "INFO")
  -version
    	Prints version and exit.
  -web.listen-address string
    	Address to listen on for web interface and telemetry. (default ":9201")
  -web.telemetry-path string
    	Path under which to expose metrics. (default "/metrics")
```

## Metrics

* `gitlab_license_exporter_last_scrape_error`
* `gitlab_license_exporter_license_active_users`
* `gitlab_license_exporter_license_addon`
* `gitlab_license_exporter_license_created_at_seconds`
* `gitlab_license_exporter_license_expired`
* `gitlab_license_exporter_license_expires_at_seconds`
* `gitlab_license_exporter_license_historical_max`
* `gitlab_license_exporter_license_info`
* `gitlab_license_exporter_license_maximum_user_count`
* `gitlab_license_exporter_license_overage`
* `gitlab_license_exporter_license_starts_at_seconds`
* `gitlab_license_exporter_license_user_limit`
* `gitlab_license_exporter_licensee_info`
* `gitlab_license_exporter_scrapes_total`

## Artefacts

* [Docker Images](https://hub.docker.com/repository/docker/leominov/gitlab_license_exporter)
* [Binaries](https://github.com/leominov/gitlab_license_exporter/releases)
