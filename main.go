package main

import (
	"flag"
	"fmt"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
)

var (
	listenAddress  = flag.String("web.listen-address", ":9201", "Address to listen on for web interface and telemetry.")
	metricsPath    = flag.String("web.telemetry-path", "/metrics", "Path under which to expose metrics.")
	logLevel       = flag.String("log-level", "INFO", "Level of logging.")
	printsVersion  = flag.Bool("version", false, "Prints version and exit.")
	collectVersion = flag.Bool("collect-version", false, "Collect GitLab version information.")
)

func main() {
	flag.Parse()

	if *printsVersion {
		fmt.Println(Version)
		return
	}

	// Output logs in JSON format
	logrus.SetFormatter(&logrus.JSONFormatter{})

	if level, err := logrus.ParseLevel(*logLevel); err == nil {
		logrus.SetLevel(level)
	}

	config, err := LoadConfigFromEnv()
	if err != nil {
		logrus.Fatal(err)
	}

	logrus.Infof("Staging gitlab_license_exporter %s...", Version)
	logrus.Debugf("Configuration: %s", config.String())

	logrus.Infof("Listening on address: %s", *listenAddress)
	http.Handle(*metricsPath, promhttp.Handler())

	exporter, err := NewExporter(config, *collectVersion)
	if err != nil {
		logrus.WithError(err).Fatal("Error creating an exporter")
	}

	err = prometheus.Register(exporter)
	if err != nil {
		logrus.WithError(err).Fatal("Error registering an collector")
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
             <head><title>GitLab License Exporter</title></head>
             <body>
             <h1>GitLab License Exporter</h1>
             <p><a href='` + *metricsPath + `'>Metrics</a></p>
             </body>
             </html>`))
	})
	if err := http.ListenAndServe(*listenAddress, nil); err != nil {
		logrus.WithError(err).Fatal("Error starting HTTP server")
	}
}
