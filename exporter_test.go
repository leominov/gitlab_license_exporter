package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewExporter(t *testing.T) {
	c := &Config{
		GitlabToken: "test",
	}
	_, err := NewExporter(c, true)
	assert.NoError(t, err)
	c = &Config{
		GitlabUsername: "test",
		GitlabPassword: "test",
	}
	_, err = NewExporter(c, true)
	assert.NoError(t, err)
	c = &Config{}
	_, err = NewExporter(c, true)
	assert.NoError(t, err)
}
