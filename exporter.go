package main

import (
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

const namespace = "gitlab_license_exporter"

type Exporter struct {
	config         *Config
	gitlabCli      *gitlab.Client
	collectVersion bool

	lastScrapeError prometheus.Gauge
	totalScrapes    prometheus.Counter
	serverInfo      *prometheus.Desc
	licenseeInfo    *prometheus.Desc
	licenseInfo     *prometheus.Desc
	licenseAddOn    *prometheus.Desc
	license         map[string]prometheus.Gauge
}

func NewExporter(c *Config, collectVersion bool) (*Exporter, error) {
	e := &Exporter{
		config:         c,
		collectVersion: collectVersion,
		totalScrapes: prometheus.NewCounter(prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "scrapes_total",
			Help:      "Current total scrapes.",
		}),
		lastScrapeError: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "last_scrape_error",
			Help:      "The last scrape error status.",
		}),
		serverInfo: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "server", "version"),
			"Server information",
			[]string{"revision", "version"},
			nil,
		),
		licenseeInfo: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "licensee", "info"),
			"Licensee information",
			[]string{"company", "email", "name"},
			nil,
		),
		licenseInfo: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "license", "info"),
			"Additional license information",
			[]string{"id", "plan"},
			nil,
		),
		licenseAddOn: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "license", "addon"),
			"License add-ons",
			[]string{"name"},
			nil,
		),
		license: map[string]prometheus.Gauge{
			"user_limit": prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: namespace,
				Name:      "license_user_limit",
				Help:      "Current license user limitation.",
			}),
			"active_users": prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: namespace,
				Name:      "license_active_users",
				Help:      "Current license active users.",
			}),
			"expired": prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: namespace,
				Name:      "license_expired",
				Help:      "Current license expired (0 - no, 1 - yes).",
			}),
			"overage": prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: namespace,
				Name:      "license_overage",
				Help:      "Current license overage.",
			}),
			"historical_max": prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: namespace,
				Name:      "license_historical_max",
				Help:      "License historical max.",
			}),
			"maximum_user_count": prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: namespace,
				Name:      "license_maximum_user_count",
				Help:      "License maximum user count.",
			}),
			"created_at": prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: namespace,
				Name:      "license_created_at_seconds",
				Help:      "License created time.",
			}),
			"starts_at": prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: namespace,
				Name:      "license_starts_at_seconds",
				Help:      "License starts time.",
			}),
			"expires_at": prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: namespace,
				Name:      "license_expires_at_seconds",
				Help:      "License expires time.",
			}),
		},
	}
	err := e.setupGitLabClient()
	if err != nil {
		return nil, err
	}
	return e, nil
}

func (e *Exporter) setupGitLabClient() error {
	var (
		cli *gitlab.Client
		err error
	)
	if len(e.config.GitlabToken) > 0 {
		cli, err = gitlab.NewClient(e.config.GitlabToken, gitlab.WithBaseURL(e.config.GitlabEndpoint))
	} else {
		cli, err = gitlab.NewBasicAuthClient(e.config.GitlabUsername, e.config.GitlabPassword, gitlab.WithBaseURL(e.config.GitlabEndpoint))
	}
	e.gitlabCli = cli
	return err
}

func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	ch <- e.totalScrapes.Desc()
	ch <- e.lastScrapeError.Desc()

	ch <- e.licenseeInfo
	ch <- e.licenseInfo
	ch <- e.licenseAddOn

	for _, m := range e.license {
		ch <- m.Desc()
	}
}

func (e *Exporter) scrape(ch chan<- prometheus.Metric) {
	e.totalScrapes.Inc()
	e.lastScrapeError.Set(0)

	lic, _, err := e.gitlabCli.License.GetLicense()
	if err != nil {
		logrus.WithError(err).Error("Failed to scrape license info")
		e.lastScrapeError.Set(1)
		return
	}
	if lic == nil || lic.ID == 0 {
		logrus.Error("Empty response from server")
		e.lastScrapeError.Set(1)
		return
	}

	e.license["user_limit"].Set(float64(lic.UserLimit))
	e.license["active_users"].Set(float64(lic.ActiveUsers))

	if lic.Expired {
		e.license["expired"].Set(1)
	} else {
		e.license["expired"].Set(0)
	}

	e.license["overage"].Set(float64(lic.Overage))
	e.license["historical_max"].Set(float64(lic.HistoricalMax))
	e.license["maximum_user_count"].Set(float64(lic.MaximumUserCount))

	if lic.CreatedAt != nil {
		e.license["created_at"].Set(float64(lic.CreatedAt.Unix()))
	}

	if lic.StartsAt != nil {
		e.license["starts_at"].Set(float64(time.Time(*lic.StartsAt).Unix()))
	}

	if lic.ExpiresAt != nil {
		e.license["expires_at"].Set(float64(time.Time(*lic.ExpiresAt).Unix()))
	}

	ch <- prometheus.MustNewConstMetric(e.licenseInfo, prometheus.GaugeValue, 1, []string{
		strconv.Itoa(lic.ID),
		lic.Plan,
	}...)

	ch <- prometheus.MustNewConstMetric(e.licenseeInfo, prometheus.GaugeValue, 1, []string{
		lic.Licensee.Company,
		lic.Licensee.Email,
		lic.Licensee.Name,
	}...)

	// ref: https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/models/license.rb
	for addOnName, addOnVal := range map[string]int{
		"GitLab_Auditor_User": lic.AddOns.GitLabAuditorUser,
		"GitLab_DeployBoard":  lic.AddOns.GitLabDeployBoard,
		"GitLab_FileLocks":    lic.AddOns.GitLabFileLocks,
		"GitLab_Geo":          lic.AddOns.GitLabGeo,
		"GitLab_ServiceDesk":  lic.AddOns.GitLabServiceDesk,
	} {
		ch <- prometheus.MustNewConstMetric(e.licenseAddOn, prometheus.GaugeValue, float64(addOnVal), addOnName)
	}

	if !e.collectVersion {
		return
	}

	ver, _, err := e.gitlabCli.Version.GetVersion()
	if err != nil {
		logrus.WithError(err).Error("Failed to scrape version info")
		e.lastScrapeError.Set(1)
		return
	}

	ch <- prometheus.MustNewConstMetric(e.serverInfo, prometheus.GaugeValue, 1, []string{
		ver.Revision,
		ver.Version,
	}...)
}

func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	e.scrape(ch)

	ch <- e.totalScrapes
	ch <- e.lastScrapeError

	for _, m := range e.license {
		ch <- m
	}
}
