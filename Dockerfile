FROM golang:1.14-alpine3.11 as builder
WORKDIR /go/src/github.com/leominov/gitlab_license_exporter
COPY . .
RUN go build -o gitlab_license_exporter ./

FROM alpine:3.11
COPY --from=builder /go/src/github.com/leominov/gitlab_license_exporter/gitlab_license_exporter /usr/local/bin/gitlab_license_exporter
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENTRYPOINT ["gitlab_license_exporter"]
